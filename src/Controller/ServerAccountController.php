<?php

namespace App\Controller;

use App\Entity\ServerAccount;
use App\Manager\FileManager;
use App\Manager\MapDataTypes;
use App\Manager\Migration;
use App\Manager\ServerAccountManager;
use App\Manager\ServerMigration;
use App\Metier\MigrateCreateQuery;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * Class ServerAccountController
 * @package App\Controller
 *
 * @Rest\RouteResource("SeverAccount")
 */
class ServerAccountController extends AbstractController
{

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the created of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ServerAccount::class)
     *     )
     * )
     * @Rest\Get("/severaccounts")
     */
    public function cgetAction(ServerAccountManager $accountManager)
    {
        dump($accountManager->loadServerAccounts());
        die;
    }


    /**
     * @param ServerAccountManager $accountManager
     * @param $id
     *
     * @Rest\Get("/severaccount/{id}")
     */

    public function getAction(ServerAccountManager $accountManager, $id)
    {
        dump($accountManager->loadServerAccount($id));
        die;
    }

    /**
     * @Rest\Get("/severaccount")
     */
    public function getAction1(ServerAccountManager $accountManager)
    {
        dump($accountManager->loadServerAccount1());
        die;
    }

    /**
     * @Rest\Get("/severaccounte")
     */
    public function getAction2(ServerAccountManager $accountManager)
    {
        dump($accountManager->loadServerAccount2());
        die;
    }

    /**
     * @param  $types
     * @Rest\Get("/dataTypes/{types}")
     */
    public function getAction3($types)
    {
        dump(MapDataTypes::map($types));
        die;
    }

    /**
     * @param  $colunm
     * @Rest\Get("/mapColunm/{colunm}")
     */
    public function getAction4($colunm)
    {
        dump(Migration::migrateColunm($colunm));
        die;
    }

    /**
     * @param  $sqlQuery
     * @Rest\Get("/mapQuery/{sqlQuery}")
     */
    public function getAction5($sqlQuery)
    {
        dump(Migration::migrateCreateTable($sqlQuery));
        die;

    }


    /**
     * @param $colunms
     * @param $tableName
     * @Rest\Get("/mapIndex/{$colunms,$tableName}")

     */
    public function getAction6($colunms, $tableName)
    {
        dump(Migration::migrateIndexes($colunms,$tableName));
        die;
    }

    /**
     * @param  $sqlQuery
     * @Rest\Get("/mapAlterTable/{sqlQuery}")
     */
    public function getAction7($sqlQuery)
    {
        dump(Migration::mapAlterTable($sqlQuery));
        die;

    }
    /**
     * @Rest\Get("/manipFile")
     */
    public function getAction8()
    {
        dump(FileManager::manipFile());
        die;

    }
    /**
     * @Rest\Get("/readFile")
     */
    public function getAction9()
    {
   dump(FileManager::readFile());

   die;
    }
    /**
     * @param  $line
     * @Rest\Get("/test/{line}")
     */
    public function getAction10($line)
    {
        dump(FileManager::isValidLineFile($line));

        die;
    }
    /**
     * @param  $lines
     * @Rest\Get("/cleanLine/{lines}")
     */
    public function getAction11($lines)
    {
        dump(FileManager::cleanLineFile($lines));

        die;
    }
    /**
     * @param  $query
     * @Rest\Get("/mapReplaceQuery/{query}")
     */
    public function getAction12($query)
    {
        dump(Migration::migrateReplaceQuery($query));

        die;
    }

    /**
     * @Rest\Get("/connectDataBase")
     */
    public function getAction13()
    {
        dump(ServerMigration::connectServer());
        die;
    }


    /**@param $sqlQuery
     * @Rest\Get("/connectDataBase/{sqlQuery}")
     */

    public function getAction14($sqlQuery)
    {
        $MigrateCreateQuery=new MigrateCreateQuery();
        dump($MigrateCreateQuery->migrateCreateTable($sqlQuery));
        die;
    }


}