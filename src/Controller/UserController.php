<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 20/03/18
 * Time: 09:37
 */

namespace App\Controller;

use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Nelmio\ApiDocBundle\Annotation as Doc;


/**
 * Class UserController
 * @package App\Controller
 *
 * @Rest\RouteResource("User")
 */
class UserController extends AbstractController
{

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the created of an user",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=User::class)
     *     )
     * )
     */
    public function postAction()
    {

    }

}