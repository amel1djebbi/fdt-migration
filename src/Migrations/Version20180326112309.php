<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180326112309 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE log_migration (id INT AUTO_INCREMENT NOT NULL, migration_id INT DEFAULT NULL, num_exec INT NOT NULL, date_exec DATE NOT NULL, date_fin_exec DATE NOT NULL, nbr_query_success INT NOT NULL, nbr_query_failure INT NOT NULL, nbr_query_warning INT NOT NULL, exec_status VARCHAR(255) NOT NULL, INDEX IDX_D51E294C79D9816F (migration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE migration (id INT AUTO_INCREMENT NOT NULL, strategy_id INT DEFAULT NULL, data_base_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, id_source INT NOT NULL, type_source VARCHAR(255) NOT NULL, encoding_tpe VARCHAR(255) NOT NULL, mysql_version VARCHAR(255) NOT NULL, postgresql_version VARCHAR(255) NOT NULL, File_idFile INT DEFAULT NULL, INDEX IDX_C31CB5C2D5CAD932 (strategy_id), UNIQUE INDEX UNIQ_C31CB5C2C1E9C1E6 (File_idFile), INDEX IDX_C31CB5C2D6C89B0 (data_base_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, file_name VARCHAR(255) NOT NULL, file_type VARCHAR(255) NOT NULL, file_path VARCHAR(255) NOT NULL, Migration_idMigration INT DEFAULT NULL, UNIQUE INDEX UNIQ_8C9F3610D818C317 (Migration_idMigration), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE view (id INT AUTO_INCREMENT NOT NULL, table_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_FEFDAB8EECFF285C (table_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `table` (id INT AUTO_INCREMENT NOT NULL, data_base_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_F6298F46D6C89B0 (data_base_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_base (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE strategy (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, type_stategy VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE StrategyDetail (Strategy_idStrategy INT NOT NULL, SubStrategy_idSubStrategy INT NOT NULL, INDEX IDX_7B57D155C3618EC2 (Strategy_idStrategy), INDEX IDX_7B57D1556A7FD3B6 (SubStrategy_idSubStrategy), PRIMARY KEY(Strategy_idStrategy, SubStrategy_idSubStrategy)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE log_detail (id INT AUTO_INCREMENT NOT NULL, log_migration_id INT DEFAULT NULL, query_status VARCHAR(255) NOT NULL, query_message VARCHAR(255) NOT NULL, INDEX IDX_3BFC2C31A18813C9 (log_migration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_strategy (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, name_strategy VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE server_account (id INT AUTO_INCREMENT NOT NULL, data_base_id INT DEFAULT NULL, hote VARCHAR(255) NOT NULL, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, INDEX IDX_F484D18D6C89B0 (data_base_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE log_migration ADD CONSTRAINT FK_D51E294C79D9816F FOREIGN KEY (migration_id) REFERENCES migration (id)');
        $this->addSql('ALTER TABLE migration ADD CONSTRAINT FK_C31CB5C2D5CAD932 FOREIGN KEY (strategy_id) REFERENCES strategy (id)');
        $this->addSql('ALTER TABLE migration ADD CONSTRAINT FK_C31CB5C2C1E9C1E6 FOREIGN KEY (File_idFile) REFERENCES file (id)');
        $this->addSql('ALTER TABLE migration ADD CONSTRAINT FK_C31CB5C2D6C89B0 FOREIGN KEY (data_base_id) REFERENCES data_base (id)');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F3610D818C317 FOREIGN KEY (Migration_idMigration) REFERENCES migration (id)');
        $this->addSql('ALTER TABLE view ADD CONSTRAINT FK_FEFDAB8EECFF285C FOREIGN KEY (table_id) REFERENCES `table` (id)');
        $this->addSql('ALTER TABLE `table` ADD CONSTRAINT FK_F6298F46D6C89B0 FOREIGN KEY (data_base_id) REFERENCES data_base (id)');
        $this->addSql('ALTER TABLE StrategyDetail ADD CONSTRAINT FK_7B57D155C3618EC2 FOREIGN KEY (Strategy_idStrategy) REFERENCES strategy (id)');
        $this->addSql('ALTER TABLE StrategyDetail ADD CONSTRAINT FK_7B57D1556A7FD3B6 FOREIGN KEY (SubStrategy_idSubStrategy) REFERENCES sub_strategy (id)');
        $this->addSql('ALTER TABLE log_detail ADD CONSTRAINT FK_3BFC2C31A18813C9 FOREIGN KEY (log_migration_id) REFERENCES log_migration (id)');
        $this->addSql('ALTER TABLE server_account ADD CONSTRAINT FK_F484D18D6C89B0 FOREIGN KEY (data_base_id) REFERENCES data_base (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE log_detail DROP FOREIGN KEY FK_3BFC2C31A18813C9');
        $this->addSql('ALTER TABLE log_migration DROP FOREIGN KEY FK_D51E294C79D9816F');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F3610D818C317');
        $this->addSql('ALTER TABLE migration DROP FOREIGN KEY FK_C31CB5C2C1E9C1E6');
        $this->addSql('ALTER TABLE view DROP FOREIGN KEY FK_FEFDAB8EECFF285C');
        $this->addSql('ALTER TABLE migration DROP FOREIGN KEY FK_C31CB5C2D6C89B0');
        $this->addSql('ALTER TABLE `table` DROP FOREIGN KEY FK_F6298F46D6C89B0');
        $this->addSql('ALTER TABLE server_account DROP FOREIGN KEY FK_F484D18D6C89B0');
        $this->addSql('ALTER TABLE migration DROP FOREIGN KEY FK_C31CB5C2D5CAD932');
        $this->addSql('ALTER TABLE StrategyDetail DROP FOREIGN KEY FK_7B57D155C3618EC2');
        $this->addSql('ALTER TABLE StrategyDetail DROP FOREIGN KEY FK_7B57D1556A7FD3B6');
        $this->addSql('DROP TABLE log_migration');
        $this->addSql('DROP TABLE migration');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE view');
        $this->addSql('DROP TABLE `table`');
        $this->addSql('DROP TABLE data_base');
        $this->addSql('DROP TABLE strategy');
        $this->addSql('DROP TABLE StrategyDetail');
        $this->addSql('DROP TABLE log_detail');
        $this->addSql('DROP TABLE sub_strategy');
        $this->addSql('DROP TABLE server_account');
    }
}
