<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 18/04/18
 * Time: 09:06
 */

namespace App\Command;
use App\Migration\MigrationByFile;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;


class  MigrationFile extends Command
{


    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('fdt:molo')
            // the short description shown while running "php bin/console list"
            ->setDescription('migration database from file .')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to migration database...');
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'migration execute',
            '============',
            '',
        ]);
        //source type
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please choice  your migration source (default = "file")',
            array('file', 'host'),
            0
        );
        $question->setErrorMessage('source %s is invalid.');

        $source = $helper->ask($input, $output, $question);
        if ($source == 'file') {
            $question = new Question('Please enter the file path :', '');
            $sourcePath = $helper->ask($input, $output, $question);
            //   echo $sourcePath;
        } elseif ($source == 'host') {
            $question = new Question('Please enter the servername :', '');
            $servername = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the username :', '');
            $username = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the password :', '');
            $password = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the database name :', '');
            $dbName = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the file path to export database :', '');
            //   $mysqlExportPath = $helper->ask($input, $output, $question);

            // ServerMigration::connectServerMysql($servername,$username,$password,$dbName,$mysqlExportPath);
        }

        //destination type
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please choice  your migration destination (defaults to file)',
            array('file', 'host'),
            0
        );
        $question->setErrorMessage('destination %s is invalid.');

        $target = $helper->ask($input, $output, $question);
        if ($target == 'file') {
            $question = new Question('Please enter the file path :', '');
            $targetPath = $helper->ask($input, $output, $question);
            //  echo $targetPath;
        } elseif ($target == 'host') {
            $question = new Question('Please enter the servername :', '');
            $servername = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the username :', '');
            $username = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the password :', '');
            $password = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the database name :', '');
            $dbName = $helper->ask($input, $output, $question);
            $question = new Question('Please enter the file path to export database :', '');
            // $mysqlExportPath = $helper->ask($input, $output, $question);

            //    ServerMigration::connectServerMysql($servername,$username,$password,$dbName,$mysqlExportPath);

        }
        // strategy type
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please choice  your strategy type (default = "structure and data")',
            array('structure', 'data', 'structure and data'),
            1
        );
        $question->setErrorMessage('strategy %s is invalid.');


        $strategy = $helper->ask($input, $output, $question);
        $migrateFile=new MigrationByFile();
        $migrateFile->$migrateFile($targetPath,$sourcePath,$strategy);


    }}
