<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 29/03/18
 * Time: 08:45
 */

namespace App\Manager;


class MapIntType
{

    /**
     * The purpose of explicit private constructor is
     * to prevent an instance initialization.
     *
     * @param void
     */
    private function __construct()
    {
        // No code should be put here.
    }
    /**
     * Dictionary of MySql data types with corresponding PostgreSql data types.
     *
     * @var array
     */
    private static $arrTypesMap = [
        'tinyint' => [
            'increased_size'           => 'int',
            'type'                     => 'smallint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'smallint' => [
            'increased_size'           => 'int',
            'type'                     => 'smallint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'mediumint' => [
            'increased_size'           => 'bigint',
            'type'                     => 'int',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'int' => [
            'increased_size'           => 'bigint',
            'type'                     => 'int',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'bigint' => [
            'increased_size'           => 'bigint',
            'type'                     => 'bigint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'float' => [
            'increased_size'           => 'double precision',
            'type'                     => 'real',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'double' => [
            'increased_size'           => 'double precision',
            'type'                     => 'double precision',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'double precision' => [
            'increased_size'           => 'double precision',
            'type'                     => 'double precision',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'numeric' => [
            'increased_size'           => '',
            'type'                     => 'numeric',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'decimal' => [
            'increased_size'           => '',
            'type'                     => 'decimal',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'decimal(19,2)' => [
            'increased_size'           => 'numeric',
            'type'                     => 'money',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],


       ];
    /**
     * Translate mysql data types into postgresql data types.
     *
     * @param  string $strMySqlDataType
     * @return string
     */
    public static function mapInt($strMySqlDataType)
    {
        $strRetVal                  = '';
        $arrDataTypeDetails         = explode(' ', $strMySqlDataType);
        $boolIncreaseOriginalSize   = in_array('UNSIGNED', $arrDataTypeDetails) || in_array('zerofill', $arrDataTypeDetails);
        $strMySqlDataType           = $arrDataTypeDetails[0];
        $strMySqlDataType           = strtolower($strMySqlDataType);
        $parenthesesFirstOccurrence = strpos($strMySqlDataType, '(');
        $parenthesesLastOccurrence  = true;
        if (false === $parenthesesFirstOccurrence) {
            // No parentheses detected.
            $strRetVal = $boolIncreaseOriginalSize
                ? self::$arrTypesMap[$strMySqlDataType]['increased_size']
                : self::$arrTypesMap[$strMySqlDataType]['type'];
        } else {
            // Parentheses detected.
            $parenthesesLastOccurrence = strpos($strMySqlDataType, ')');
            $arrDataType               = explode('(', $strMySqlDataType);
            $strDataType               = strtolower($arrDataType[0]);

            if ('decimal' == $strDataType || 'numeric' == $strDataType) {
                $strRetVal = self::$arrTypesMap[$strDataType]['type'] . '(' . $arrDataType[1];
            } elseif ('decimal(19,2)' == $strMySqlDataType) {
                $strRetVal = $boolIncreaseOriginalSize
                    ? self::$arrTypesMap[$strDataType]['increased_size']
                    : self::$arrTypesMap[$strDataType]['type'];
            } elseif (self::$arrTypesMap[$strDataType]['mySqlVarLenPgSqlFixedLen']) {
                // Should be converted without a length definition.
                $strRetVal = $boolIncreaseOriginalSize
                    ? self::$arrTypesMap[$strDataType]['increased_size']
                    : self::$arrTypesMap[$strDataType]['type'];
            } else {
                // Should be converted with a length definition.
                $strRetVal = $boolIncreaseOriginalSize
                    ? self::$arrTypesMap[$strDataType]['increased_size'] . '(' . $arrDataType[1]
                    : self::$arrTypesMap[$strDataType]['type'] . '(' . $arrDataType[1];
            }
            // Prevent incompatible length (CHARACTER(0) or CHARACTER VARYING(0)).
            }

        return ' ' . strtoupper($strRetVal) . ' ';
    }





}