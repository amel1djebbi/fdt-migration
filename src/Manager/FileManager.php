<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 09/04/18
 * Time: 09:33
 */

namespace App\Manager;

use Exception;


class FileManager
{


    /**
     * @throws Exception
     */
    public static function readFile()
    {
        $queries = array();
        $query = '';
        $strategy = 'data';
       $source = '/home/ad/Vidéos/migrator.sql';
       $target = '/home/ad/Vidéos/target1.sql';
        $subStrategy = 'do not update data';
        @unlink($target);

        if ($fichier = @fopen($source, "r")) {
            $lines = file($source);
            foreach ($lines as $lineNumber => $lineContent) {

                $lineContent = self::cleanLineFile($lineContent);

                $lineLength = strlen($lineContent);
                $endQueryPosition = strpos($lineContent, ';');

                $query .=
                    substr($lineContent, 0, ((!$endQueryPosition) ? $lineLength : $endQueryPosition));

                if ($endQueryPosition) {
                    $queries[] = str_replace("\n", ' ', $query);
                    $query = '';


                }
            }

            $migratedQueries = [];

            foreach ($queries as $key => $query) {
                $query = preg_replace("# +#", " ", $query);
                if (count($queries) < 50) {
                    if ($strategy == 'structure') {
                        $migratedQueries[$key] = self::migrateStructure($query);
                        self::writeQueries($target, $migratedQueries);

                    } elseif ($strategy == 'data') {
                        $migratedQueries[$key] = self::migrateData($query, $subStrategy);
                        dump($migratedQueries);
                        self::writeQueries($target, $migratedQueries);
                    } elseif ($strategy == 'structure and data') {
                        $migratedQueries[$key] = self::migrateStructureAndData($query);
                        self::writeQueries($target, $migratedQueries);
                    }
                    self::writeQueries($target, $migratedQueries);
                } elseif (count($queries) == 50) {
                    $queries = array();


                }
            }
        }
    }


    public static function writeQueries($filename, array $queries)
    {
        $targetFile = fopen($filename, "w+");
        foreach ($queries as $query) {
            $query = $query . PHP_EOL;

            fwrite($targetFile, $query);

        }
        fclose($targetFile);
    }


    /**
     * @param $line
     * @return bool
     */
    public static function isValidLineFile($line)
    {
        $pattern = '/^SET/';

        if ((stripos($line, '/*') !== false) ||
            (stripos($line, '--') !== false) ||
            (preg_match($pattern, $line)) ||
            (preg_match('/^UNLOCK/', $line)) ||
            (preg_match('/^LOCK/', $line)) ||
            (!strlen($line))) {

            RETURN false;

        } else
            RETURN true;
    }


    /**
     * @param $line
     * @return string
     */
    public static function cleanLineFile($line)
    {
        if (stripos($line, ') ENGINE') !== false) {
            $line = trim(str_ireplace($line, ');', $line));
            return $line;
        } elseif (self::isValidLineFile($line) == true) {
            return $line;
        } else {
            $line = trim(str_ireplace($line, '', $line));
            return $line;
        }


    }

    /**
     * @param $query
     * @return string
     */
    public static function migrateStructure($query)
    {
        if (stripos($query, 'create table') !== false) {
            $query = Migration::migrateCreateTable($query);

        } elseif (stripos($query, 'alter table') !== false) {
            $query = Migration::mapAlterTable($query);

        } elseif (stripos($query, 'drop table') !== false) {
            $query = Migration::migrateDropTable($query);

        } else

            return '';

        return $query;
    }

    /**
     * @param $query
     * @param $subStrategy
     * @return string
     */
    public static function migrateData($query, $subStrategy)
    {
        $query = str_replace('`', '', $query);
        if (stripos($query, 'insert into ') !== false) {
            $tableName = self::extractInsertTableName($query);

            $query1 = fileManager::truncateTable($tableName);
            $query = self::ChoiceSubStrategy($query, $subStrategy);
        } elseif (stripos($query, 'update') !== false) {//to do
            $tableName = self::getUpdateTableName($query);
            $query1 = fileManager::truncateTable($tableName);
            $query = Migration::migrateUpdateQuery($query);

        } elseif (stripos($query, 'replace into ') !== false) {
            $tableName = self::extractInsertTableName($query);
            $query1 = fileManager::truncateTable($tableName);
            $query = self::ChoiceSubStrategy($query, $subStrategy);

        } else

            return '';
        return $query1 . PHP_EOL . $query;
    }

    public static function migrateStructureAndData($query)
    {
        if (stripos($query, 'insert into') !== false) {
            $query = Migration::migrateInsertQuery($query);

        } elseif (stripos($query, 'update') !== false) {
            $query = Migration::migrateUpdateQuery($query);


        } elseif (stripos($query, 'replace into ') !== false) {
            $query = Migration::migrateReplaceQuery($query);

        }
        if (stripos($query, 'create table') !== false) {
            $query = Migration::migrateCreateTable($query);

        } elseif (stripos($query, 'alter table') !== false) {
            $query = Migration::mapAlterTable($query);

        } elseif (stripos($query, 'drop table') !== false) {
            $query = Migration::migrateDropTable($query);

        }


        return $query;
    }

    public static function truncateTable($tableName)
    {
        return 'TRUNCATE TABLE ' . $tableName . ' ;';
    }

    public static function extractInsertTableName($query)
    {
        $createFieldPos = strpos($query, "(");

        $createTableStr = trim(substr($query, 0, $createFieldPos));

        $tableName = trim(substr($createTableStr, strrpos($createTableStr, ' ')));
        return $tableName;
    }

    public static function getUpdateTableName($query)
    {
        $createFieldPos = stripos($query, "set");

        $createTableStr = trim(substr($query, 0, $createFieldPos));

        $tableName = trim(substr($createTableStr, strrpos($createTableStr, ' ')));


        return $tableName;
    }


    public static function ChoiceSubStrategy($query, $subStrategy)
    {
        if ($subStrategy == 'do not update data') {
            $query = rtrim(Migration::migrateInsertQuery($query), ';');
            $query = $query . ' ON CONFLICT DO nothing ; ';
        } else {
            $query = Migration::migrateInsertQuery($query);
        }

        if ($subStrategy == 'update data') {

            $query = Migration::migrateInsertQuery($query);


        }
        return $query;

    }

    public static function findIndexUnique($query, $tableName)/// to do
    {
        if (stripos($query, $tableName))
            return $query;
    }


}






