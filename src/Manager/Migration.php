<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 30/03/18
 * Time: 15:09
 */

namespace App\Manager;

use Exception;

class Migration
{
    /**
     * The purpose of explicit private constructor is
     * to prevent an instance initialization.
     * @param void
     */



    /**
     * @param $sqlQuery
     * @return mixed|string
     * @throws Exception
     */
    public static function migrateCreateTable($sqlQuery)
    {

        $sqlQuery = str_replace('`', '', $sqlQuery);


        $sqlQuery = preg_replace("# +#", " ", $sqlQuery);

        if (strpos($sqlQuery, '(') === false || strpos($sqlQuery, ')') === false) {
            throw new Exception(' syntax error');

        }

        $createFieldPos = strpos($sqlQuery, "(");

        $createTableStr = trim(substr($sqlQuery, 0, $createFieldPos));

        $tableName = trim(substr($createTableStr, strrpos($createTableStr, ' ')));
        /* if ($tableName=='EXISTS'|| $tableName=='' || $tableName=='table'  || $tableName=='if' || $tableName='not')
         {
             throw new Exception(' syntax error :you shouls add table name');

         }
     */
        $createTableStr = rtrim($createTableStr, $tableName);
        $createFieldsStr = trim(substr($sqlQuery, $createFieldPos + 1));


        $createFieldsStr = rtrim($createFieldsStr, ';');

        $createFieldsStr = substr($createFieldsStr, 0, -1);

        $fields = explode(',', $createFieldsStr);


        foreach ($fields as $key => $field) {


            if ((strpos($field, 'index') === false) &&
                (strpos($field, 'primary key') === false) &&
                (strpos($field, 'foreign key') === false)) {
                $field = self::migrateColunm($field);

                $fields[$key] = $field;
            } elseif (strpos($field, 'index') !== false) {

                $field = self::migrateIndexes($field, $tableName);
                $fields[$key] = $field;
                $fieldsIndex = $fields;
            } elseif (strpos($field, 'primary key') === true || strpos($field, 'foreign key ') === true) {
                $field = $field;
                $fields1[$key] = $field;
            }


        }

        $reqSql = sprintf($createTableStr . ' %s (%s;', $tableName, implode(', ', $fields));

        if (stripos($reqSql, ', create') !== false) {
            $reqSqlTemp = str_ireplace(', create', ' ); create', $reqSql);


            $reqSql = $reqSqlTemp;
            $arraySql = explode(';', $reqSql);
            $reqSql = implode(';' . PHP_EOL, $arraySql);

            return $reqSql;
        } else

            return $reqSql;

    }


    /**
     * @param $colunm
     * @return string
     */
    public static function migrateColunm($colunm)
    {


        $colunm = str_ireplace(['UNSIGNED ZEROFILL', 'UNSIGNED'], '', $colunm);
        $posCollate = stripos($colunm, 'collate');
        if ($posCollate !== false) {
            $encodingColunm = substr($colunm, $posCollate);
            $encodingColunm = preg_replace("# +#", " ", trim($encodingColunm));

            $encodeArray = explode(' ', $encodingColunm);
            $encoding = $encodeArray[1];


            $colunm = str_ireplace(['collate', $encoding], '', $colunm);

        }

        $colunm = preg_replace("# +#", " ", trim($colunm));


        $nameColunm = strstr($colunm, ' ', true);

        $typeAndConstraint = trim(strstr($colunm, ' '));


        if (strpos($typeAndConstraint, '(') !== false) {

            $type = strstr($typeAndConstraint, '(', true);

            $sizeAndConstraint = strstr($typeAndConstraint, '(');
            if (stripos($sizeAndConstraint, ' ') !== false) {
                $size = strstr($sizeAndConstraint, ' ', true);
                $Constraint = trim(trim($sizeAndConstraint, $size));
            } else {
                $size = $sizeAndConstraint;

                $Constraint = '';
            }
            $type = $type . $size;

            $types = MapDataTypes::map($type);


        } else {


            $type = strstr($typeAndConstraint, ' ', true);

            $Constraint = trim($typeAndConstraint, $type);

            $types = MapDataTypes::map($type);


        }


        return $nameColunm . $types . $Constraint;
    }


    /**
     * @param $colunm
     * @param $tableName
     * @return string
     */
    public static function migrateIndexes($colunm, $tableName)
    {
        $colunm = trim($colunm, 'index');
        $colunm = trim(preg_replace("# +#", " ", trim($colunm)));

        $arrayCut = explode(' ', $colunm);
        $createFieldPos = strpos($colunm, "(");

        $createIndexStr = trim(substr($colunm, 0, $createFieldPos));
        $fieldName = trim(substr($colunm, strrpos($colunm, ' ')));


        $index = 'CREATE ' . $createIndexStr . ' on ' . $tableName . $fieldName;

        return $index;
    }

    /**
     * @param string $sqlQuery
     * @return string
     *
     */

    public static function mapAlterTable($sqlQuery)
    {
        $sqlQuery = str_replace('`', '', $sqlQuery);

        $sqlQuery = trim(preg_replace("# +#", " ", trim($sqlQuery)));
        $alterTableStr = trim(strstr($sqlQuery, 'ADD', true));
        $tableName = trim(substr($alterTableStr, strrpos($alterTableStr, ' ')));
        $constraints = strstr($sqlQuery, 'ADD');

        $arrayConstraint = explode(',', $constraints);
        foreach ($arrayConstraint as $key => $constraint) {
            $constraint = trim(trim($constraint), 'ADD');

            if (stripos($constraint, 'unique key') !== false || stripos($constraint, 'index') !== false || stripos($constraint, 'unique index') !== false) {

                $constraint = self::migrateIndexes($constraint, $tableName);

                $arrayConstraint[$key] = $constraint;


            }
        }

        $reqSql = 'alter table' .$tableName.' ' . implode(', ', $arrayConstraint);
        if (stripos($reqSql, ', create') !== false) {
            $reqSqlTemp = str_ireplace(', create', ' ;CREATE', $reqSql);


            $reqSql = $reqSqlTemp;
            $arraySql = explode(';', $reqSql);
            $reqSql = trim(implode(';' . PHP_EOL, $arraySql));

            return $reqSql . ';';


        } else
            return $reqSql;

    }

    /**
     * @param $sqlQuery
     * @return string
     */
    public static function migrateUpdateQuery($sqlQuery)
    {
        $sqlQuery = str_replace('`', '', $sqlQuery);

        $sqlQuery = trim(preg_replace("# +#", " ", trim($sqlQuery)));
        return $sqlQuery . ';';
    }

    /**
     * @param $sqlQuery
     * @return string
     */
    public static function migrateInsertQuery($sqlQuery)
    {
        $sqlQuery = str_replace('`', '', $sqlQuery);

        $sqlQuery = trim(preg_replace("# +#", " ", trim($sqlQuery)));
        return $sqlQuery . ';';
    }

    /**
     * @param $sqlQuery
     * @return string
     */

    public static function migrateReplaceQuery($sqlQuery)
    {
        $sqlQuery = str_replace('`', '', $sqlQuery);

        $sqlQuery = trim(preg_replace("# +#", " ", trim($sqlQuery)));

        $arraySql = explode(' ', $sqlQuery);

        $tableName = $arraySql[2];
        $colunmName = trim(strstr(trim(substr($sqlQuery, strpos($sqlQuery, '('))), ')', true));
        $colunmValue = strstr(strstr($sqlQuery, 'VALUES'), '(');
        $pattern = '/^[(][a-z]|[A-Z]/';

        if (preg_match($pattern, $colunmName)) {

            return 'INSERT INTO ' . $tableName . ' ' . $colunmName . ')' . ' VALUES ' . $colunmValue . ';';
        } else
            return 'INSERT INTO ' . $tableName . ' ' . ' VALUES ' . $colunmValue . ' ;';


    }

    /**
     * @param $sqlQuery
     * @return string
     */

    public static function migrateDropTable($sqlQuery)
    {
        $sqlQuery = str_replace('`', '', $sqlQuery);

        $sqlQuery = trim(preg_replace("# +#", " ", trim($sqlQuery)));
        return $sqlQuery . ';';
    }

    public static function findUniqueIndex($colunm, $tableName)
    {

        $colunm = trim($colunm, 'unique index');
        $colunm = trim(preg_replace("# +#", " ", trim($colunm)));

        $arrayCut = explode(' ', $colunm);
        $createFieldPos = strpos($colunm, "(");

        $createIndexStr = trim(substr($colunm, 0, $createFieldPos));
        $fieldName = trim(substr($colunm, strrpos($colunm, ' ')));


        $index = 'CREATE ' . $createIndexStr . ' on ' . $tableName . $fieldName;

        return $index;
    }


}
