<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 12/04/18
 * Time: 16:49
 */

namespace App\Manager;

use PDO;
use PDOException;
use Exception;
class ServerMigration
{


    public  function connectServerMysql($servername,$username,$password,$dbName,$mysqlExportPath)
{

    self::exportDataBase($dbName, $username, $password, $servername, $mysqlExportPath);

}

    public  function connectMysqlServer($servername,$username,$password,$dbName)
    {

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully";

        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public  function exportDataBase($dbName, $username, $password, $servername, $mysqlExportPath)
    {
        self::connectMysqlServer($servername,$username,$password,$dbName);

        $output = array();
        $command = 'mysqldump -h' . $servername . ' -u' . $username . ' -p' . $password . ' ' . $dbName . ' > ' . $mysqlExportPath;
        exec($command, $output, $worked);
        switch ($worked) {
            case 0:
                echo 'Database <b>' . $dbName . '</b> successfully exported to <b>' . getcwd() . '/' . $mysqlExportPath . '</b>';
                break;
            case 1:
                echo 'There was a warning during the export of <b>' . $dbName . '</b> to <b>' . getcwd() . '/' . $mysqlExportPath . '</b>';
                break;
            case 2:
                echo 'There was an error during export. Please check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' . $dbName . '</b></td></tr><tr><td>MySQL User Name:</td><td><b>' . $username . '</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' . $servername . '</b></td></tr></table>';
                break;
        }
    }


    public  function connectPosgresqlServer()
        {

            $host_bdd='adresse.serveur.fr';
            $name_bdd='bdthomas';
            $user_bdd='thomas';
            $pass_bdd='thomas';

            try{
                $bdd = new PDO ("pgsql:host=".$host_bdd.";dbname=".$name_bdd."", "".$user_bdd."", "".$pass_bdd."") or die(print_r($bdd->errorInfo()));
                $bdd->exec("SET NAMES utf8");
                $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            }

            catch(Exception $e){
                die("Erreur!".$e->getMessage());
            }

            return $bdd;



    }


}