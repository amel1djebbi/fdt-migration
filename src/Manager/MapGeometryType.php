<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 06/04/18
 * Time: 08:57
 */

namespace App\Manager;


class MapGeometryType
{
    /**
     * The purpose of explicit private constructor is
     * to prevent an instance initialization.
     *
     * @param void
     */
    private function __construct()
    {
        // No code should be put here.
    }

    /**
     *
     * @var array
     */
    private static $arrTypesMap = [
        'point' => [
            'increased_size'           => '',
            'type'                     => 'point',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'multipoint' => [
            'increased_size'           => '',
            'type'                     => 'point[]',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'linestring' => [
            'increased_size'           => '',
            'type'                     => 'line',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'multilinestring' => [
            'increased_size'           => '',
            'type'                     => 'line[]',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'polygon' => [
            'increased_size'           => '',
            'type'                     => 'polygon',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'multipolygon' => [
            'increased_size'           => '',
            'type'                     => 'polygon[]',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
    ];

    /**
     * Translate mysql data types into postgresql data types.
     *
     * @param  string $strMySqlDataType
     * @return string
     */
    public static function map($strMySqlDataType)
    {
        $strRetVal = '';
        $arrDataTypeDetails = explode(' ', $strMySqlDataType);
        $boolIncreaseOriginalSize = in_array('UNSIGNED', $arrDataTypeDetails) || in_array('zerofill', $arrDataTypeDetails);
        $strMySqlDataType = $arrDataTypeDetails[0];
        $strMySqlDataType = strtolower($strMySqlDataType);
        $parenthesesFirstOccurrence = strpos($strMySqlDataType, '(');
        $parenthesesLastOccurrence = true;
        if (false === $parenthesesFirstOccurrence) {
            // No parentheses detected.
            $strRetVal = $boolIncreaseOriginalSize
                ? self::$arrTypesMap[$strMySqlDataType]['increased_size']
                : self::$arrTypesMap[$strMySqlDataType]['type'];
        } else {
            // Parentheses detected.
            $parenthesesLastOccurrence = strpos($strMySqlDataType, ')');
            $arrDataType = explode('(', $strMySqlDataType);
            $strDataType = strtolower($arrDataType[0]);
            if ('enum' == $strDataType || 'set' == $strDataType) {
                $strRetVal = 'varchar(255)';
            } elseif ('decimal' == $strDataType || 'numeric' == $strDataType) {
                $strRetVal = self::$arrTypesMap[$strDataType]['type'] . '(' . $arrDataType[1];
            } elseif ('decimal(19,2)' == $strMySqlDataType) {
                $strRetVal = $boolIncreaseOriginalSize
                    ? self::$arrTypesMap[$strDataType]['increased_size']
                    : self::$arrTypesMap[$strDataType]['type'];
            } elseif (self::$arrTypesMap[$strDataType]['mySqlVarLenPgSqlFixedLen']) {
                // Should be converted without a length definition.
                $strRetVal = $boolIncreaseOriginalSize
                    ? self::$arrTypesMap[$strDataType]['increased_size']
                    : self::$arrTypesMap[$strDataType]['type'];
            } else {
                // Should be converted with a length definition.
                $strRetVal = $boolIncreaseOriginalSize
                    ? self::$arrTypesMap[$strDataType]['increased_size'] . '(' . $arrDataType[1]
                    : self::$arrTypesMap[$strDataType]['type'] . '(' . $arrDataType[1];
            }
            // Prevent incompatible length (CHARACTER(0) or CHARACTER VARYING(0)).
            switch ($strRetVal) {
                case 'character(0)':
                    $strRetVal = 'character(1)';
                    break;
                case 'character varying(0)':
                    $strRetVal = 'character varying(1)';
                    break;
            }
        }

        return ' ' . strtoupper($strRetVal) . ' ';
    }


}