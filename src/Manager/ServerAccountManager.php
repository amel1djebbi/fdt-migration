<?php

namespace App\Manager;

use App\Entity\ServerAccount;
use Doctrine\ORM\EntityManagerInterface;

class ServerAccountManager

{
    /**
     * @var \Doctrine\ORM\EntityManager $em entity manager
     */
    private $em;
    /**
     * @var \Doctrine\ORM\EntityRepository $em repository
     */
    private $repository;


    /**
     * ServerAccountManager constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(ServerAccount::class);;

    }

    /**
     * @return ServerAccount[]|array
     */
    public function loadServerAccounts()
    {
        return $this->repository->findAll();
    }

    /**
     * Load server Acooun by ID
     *
     * @param Integer $id
     * @return ServerAccount
     */
    public function loadServerAccount($id):ServerAccount
    {
        return $this->repository->find($id);
    }

    /**
     * @return ServerAccount|null|object
     */
    public function loadServerAccount1()
    {
        return $this->repository->findOneBy(['code' => '125']);
    }

    /**
     * @return ServerAccount|null|object
     */
    public function loadServerAccount2()
    {
        return $this->repository->findOneBy(['code' => '123','host' => '127.0.0.1']);
    }

    /**
     * @param $code
     * @return ServerAccount[]
     */
  /**  public function findAllGreaterThanCode($code):array{

        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.code > :code')
            ->setParameter('code', $code)
            ->orderBy('p.code', 'ASC')
            ->getQuery();

        return $qb->execute();
    }*/

}