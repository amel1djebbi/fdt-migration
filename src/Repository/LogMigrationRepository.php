<?php

namespace App\Repository;

use App\Entity\LogMigration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LogMigration|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogMigration|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogMigration[]    findAll()
 * @method LogMigration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogMigrationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LogMigration::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('l')
            ->where('l.something = :value')->setParameter('value', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
