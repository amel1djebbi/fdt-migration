<?php

namespace App\Repository;

use App\Entity\Migration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Migration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Migration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Migration[]    findAll()
 * @method Migration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MigrationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Migration::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('m')
            ->where('m.something = :value')->setParameter('value', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
