<?php

namespace App\Repository;

use App\Entity\SubStrategy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubStrategy|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubStrategy|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubStrategy[]    findAll()
 * @method SubStrategy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubStrategyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SubStrategy::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('s')
            ->where('s.something = :value')->setParameter('value', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
