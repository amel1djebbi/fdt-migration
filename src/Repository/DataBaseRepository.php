<?php

namespace App\Repository;

use App\Entity\DataBase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DataBase|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataBase|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataBase[]    findAll()
 * @method DataBase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataBaseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DataBase::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('d')
            ->where('d.something = :value')->setParameter('value', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
