<?php

namespace App\Repository;

use App\Entity\StrategyDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StrategyDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method StrategyDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method StrategyDetail[]    findAll()
 * @method StrategyDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StrategyDetailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StrategyDetail::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('s')
            ->where('s.something = :value')->setParameter('value', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
