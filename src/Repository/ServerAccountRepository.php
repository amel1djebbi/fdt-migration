<?php

namespace App\Repository;

use App\Entity\ServerAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ServerAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServerAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServerAccount[]    findAll()
 * @method ServerAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServerAccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ServerAccount::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('s')
            ->where('s.something = :value')->setParameter('value', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
