<?php

namespace App\Repository;

use App\Entity\LogDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LogDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogDetail[]    findAll()
 * @method LogDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogDetailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LogDetail::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('l')
            ->where('l.something = :value')->setParameter('value', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
