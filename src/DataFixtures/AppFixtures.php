<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 05/03/18
 * Time: 11:30
 */

namespace App\DataFixtures;

use App\Entity\Migration;
use App\Entity\ServerAccount;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {

        foreach ($this->getServerAccountData() as [$host, $code,$username, $password, $SGBDType]) {
            $serverAccount = new ServerAccount();
            $serverAccount->setHost($host);
            $serverAccount->setCode($code);
            $serverAccount->setUsername($username);
            $serverAccount->setPassword($password);
            $serverAccount->setSGBDType($SGBDType);
            $manager->persist($serverAccount);
        }
        $manager->flush();


        foreach ($this->getMigrationData() as [$label, $sourceType, $encodingType, $mysqlVersion,$postgresqlVersion]) {
            $migration = new Migration();
            $migration->setLabel($label);
            $migration->setSourceType($sourceType);
            $migration->setEncodingType($encodingType);
            $migration->setMysqlVersion($mysqlVersion);
            $migration->setPostgresqlVersion($postgresqlVersion);
            $manager->persist($migration);
        }
        $manager->flush();
    }

    private function getMigrationData(): array
    {
        return [
            // $getMigrationData = [$label,$sourceType, $encodingType, $mysqlVersion,$postgresqlVersion]) ];
            ["migration1","file", "utf-8", "5.7", "9.4"],
            ["migration2","dataBase", "utf-8", "5.5", "9.6"],
            ["migration3","file", "utf-8", "5.6", "10"],


        ];
    }
    private function getServerAccountData(): array
    {
        return [
            // $getServerAccountData = [$host, $code,$username, $password, $SGBDType];
            ["127.0.0.1","123", "root", "root", "MySQL"],
            ["192.168.7.187","124", "root", "root", "MySQL"],
            ["localhost","125","amel","amel", "MySQL"]
        ];
    }

   /* public function load(ObjectManager $manager)
    {
        // Liste des hosts de serverAccount à ajouter
        $hosts = array(
            '127.0.0.1',
            '192.0.0.2',
            'localhost'
        );

        foreach ($hosts as $host) {
            // On crée le serverAccount
            $serverAccount = new ServerAccount();
            $serverAccount->setHost($host);


            // On la persiste
            $manager->persist($serverAccount);
        }

        // On déclenche l'enregistrement de tous les serveurAccount
        $manager->flush();

}
   */
}


