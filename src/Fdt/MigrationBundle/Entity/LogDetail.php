<?php

namespace App\Fdt\MigrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LogDetailRepository")
 */
class LogDetail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $queryStatus;
    /**
     * @ORM\Column(type="string")
     */
    private $queryMessage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LogMigration", inversedBy="logDetail")
     * @ORM\JoinColumn(nullable=true)
     */
    private $logMigration;

    /**
     * @return logMigration
     */
    public function getLogMigration(): logMigration
    {
        return $this->logMigration;
    }

    /**
     * @param LogMigration $logMigration
     */
    public function setLogMigration(LogMigration $logMigration)
    {
        $this->logMigration = $logMigration;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQueryStatus()
    {
        return $this->queryStatus;
    }

    /**
     * @param mixed $queryStatus
     */
    public function setQueryStatus($queryStatus)
    {
        $this->queryStatus = $queryStatus;
    }

    /**
     * @return mixed
     */
    public function getQueryMessage()
    {
        return $this->queryMessage;
    }

    /**
     * @param mixed $queryMessage
     */
    public function setQueryMessage($queryMessage)
    {
        $this->queryMessage = $queryMessage;
    }

}



