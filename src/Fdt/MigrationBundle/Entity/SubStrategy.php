<?php

namespace App\Fdt\MigrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubStrategyRepository")
 */
class SubStrategy
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */

    private $label;
    /**
     * @ORM\Column(type="string")
     */
    private $strategyName;
    
    /**
     * @var ArrayCollection SubStrategy $strategy
     *
     * Inverse Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Strategy", mappedBy="subStrategy", cascade={"persist", "merge"})
     */
    private $strategy;

    /**
     * SubStrategy constructor.
     */
    public function __construct() {
        $this->strategy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStrategyName()
    {
        return $this->strategyName;
    }

    /**
     * @param mixed $strategyName
     */
    public function setStrategyName($strategyName)
    {
        $this->strategyName = $strategyName;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }


}
