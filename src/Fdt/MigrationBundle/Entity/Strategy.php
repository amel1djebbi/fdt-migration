<?php

namespace App\Fdt\MigrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StrategyRepository")
 */
class Strategy
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $label;
    /**
     * @ORM\Column(type="string")
     */
    private $strategyType;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Migration", mappedBy="strategy")
     */
    private $migration;
    /**
     * @var ArrayCollection $SubStrategy $SubStrategy
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\SubStrategy", inversedBy="strategy", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="StrategyDetail",
     *   joinColumns={@ORM\JoinColumn(name="strategy_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="subStrategy_id", referencedColumnName="id")}
     * )
     */
    private  $SubStrategy;

    /**
     * Strategy constructor.
     */
    public function __construct()
    {
        $this->migration = new ArrayCollection();
    }

    /**
     * @return Collection|migration]
     */
    public function getMigration()
    {
        return $this->migration;
    }


    /**
     *
     */
    public function _construct()
    {
        $this->SubStrategy;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getStrategyType()
    {
        return $this->strategyType;
    }

    /**
     * @param mixed $strategyType
     */
    public function setStrategyType($strategyType)
    {
        $this->strategyType = $strategyType;
    }

}
