<?php

namespace App\Fdt\MigrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LogMigrationRepository")
 */
class LogMigration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $numExec;
    /**
     * @ORM\Column(type="date")
     */
    private $dateExec;
    /**
     * @ORM\Column(type="date")
     */

    private $dateFinExec;
    /**
     * @ORM\Column(type="integer")
     */
    private $nbrQuerySuccess;
    /**
     * @ORM\Column(type="integer")
     */
    private $nbrQueryFailure;
    /**
     * @ORM\Column(type="integer")
     */
    private $nbrQueryWarning;
    /**
     * @ORM\Column(type="string")
     */
    private $execStatus;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LogDetail", mappedBy="logMigration")
     */
    private $logDetail;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Migration", inversedBy="logMigration")
     * @ORM\JoinColumn(nullable=true)
     */
    private $migration;

    /**
     * LogMigration constructor.
     */
    public function __construct()
    {
        $this->logDetail = new ArrayCollection();
    }

    /**
     * @return migration
     */
    public function getMigration(): migration
    {
        return $this->migration;
    }

    /**
     * @param Migration $migration
     */
    public function setMigration(Migration $migration)
    {
        $this->migration = $migration;
    }

    /**
     * @return Collection|logDetail]
     */
    public function getLogDetail()
    {
        return $this->logDetail;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNumExec()
    {
        return $this->numExec;
    }

    /**
     * @return mixed
     */
    public function getDateFinExec()
    {
        return $this->dateFinExec;
    }

    /**
     * @param mixed $dateFinExec
     */
    public function setDateFinExec($dateFinExec)
    {
        $this->dateFinExec = $dateFinExec;
    }

    /**
     * @return mixed
     */
    public function getNbrQuerySuccess()
    {
        return $this->nbrQuerySuccess;
    }

    /**
     * @param mixed $nbrQuerySuccess
     */
    public function setNbrQuerySuccess($nbrQuerySuccess)
    {
        $this->nbrQuerySuccess = $nbrQuerySuccess;
    }

    /**
     * @return mixed
     */
    public function getExecStatus()
    {
        return $this->execStatus;
    }

    /**
     * @param mixed $execStatus
     */
    public function setExecStatus($execStatus)
    {
        $this->execStatus = $execStatus;
    }

    /**
     * @return mixed
     */
    public function getDateExec()
    {
        return $this->dateExec;
    }

    /**
     * @return mixed
     */
    public function getNbrQueryWarning()
    {
        return $this->nbrQueryWarning;
    }

    /**
     * @param mixed $nbrQueryWarning
     */
    public function setNbrQueryWarning($nbrQueryWarning)
    {
        $this->nbrQueryWarning = $nbrQueryWarning;
    }

    /**
     * @return mixed
     */
    public function getNbrQueryFailure()
    {
        return $this->nbrQueryFailure;
    }

    /**
     * @param mixed $nbrQueryFailure
     */
    public function setNbrQueryFailure($nbrQueryFailure)
    {
        $this->nbrQueryFailure = $nbrQueryFailure;
    }

    /**
     * @param mixed $dateExec
     */
    public function setDateExec($dateExec)
    {
        $this->dateExec = $dateExec;
    }

    /**
     * @param mixed $numExec
     */
    public function setNumExec($numExec)
    {
        $this->numExec = $numExec;
    }



}


