<?php

namespace App\Fdt\MigrationBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 */
class File
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $fileName;
    /**
     * @ORM\Column(type="string")
     */
    private $fileType;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Migration", cascade={"persist", "merge", "remove"})
     * @ORM\JoinColumn(name="migration_id", referencedColumnName="id")
     */
    private  $migration ;

    /**
     * File constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * @param File $file
     */
    public function setMigration(Migration $migration)
    {
        $this->migration = $migration;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->File;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * @param mixed $fileType
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
    /**
     * @ORM\Column(type="string")
     */
    private $filePath;

}
