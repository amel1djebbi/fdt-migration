<?php

namespace App\Fdt\MigrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DataBaseRepository")
 */
class DataBase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Migration", mappedBy="dataBase")
     */
    private $migration;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ServerAccount", inversedBy="dataBase")
     * @ORM\JoinColumn(nullable=true)
     */
    private $serverAccount;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DataBaseTable", mappedBy="dataBase")
     */
    private $dataBaseTable;


    public function __construct()
    {
        $this->serverAccount = new ArrayCollection();
    }

    /**
     * @return serverAccount
     */
    public function getServerAccount(): serverAccount
    {
        return $this->serverAccount;
    }

    /**
     * @param ServerAccount $serverAccount
     */
    public function setServerAccount(ServerAccount $serverAccount)
    {
        $this->serverAccount = $serverAccount;
    }
    /**
     * @return Collection|migration]
     */
    public function getMigration()
    {
        return $this->migration;
    }

    /**
     *
     */
    public function __construct2()
    {
        $this->dataBaseTable = new ArrayCollection();
    }
        /**
         * @return Collection|DataBaseTable]
         *
         */

        public function getDataBaseTable()
    {
        return $this->dataBaseTable;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
