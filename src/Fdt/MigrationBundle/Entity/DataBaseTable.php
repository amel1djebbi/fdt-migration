<?php

namespace App\Fdt\MigrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TableBdRepository")
 */
class DataBaseTable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
* @ORM\Column(type="string")
*/
    private $tableName;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DataBase", inversedBy="dataBaseTable")
     * @ORM\JoinColumn(nullable=true)
     */
    private $dataBase;

    /**
     * @return dataBase
     */
    public function getDataBase(): dataBase
    {
        return $this->dataBase;
    }

    /**
     * @param DataBase $dataBase
     */
    public function setDataBase(DataBase $dataBase)
    {
        $this->dataBase = $dataBase;
    }
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\View", mappedBy="dataBaseTable")
     */
    private $view;

    /**
     * TableBd constructor.
     */
    public function __construct()
    {
        $this->view = new ArrayCollectionMigration();
    }

    /**
     * @return Collection|view]
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param mixed $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}
