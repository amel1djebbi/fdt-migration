<?php

namespace App\Fdt\MigrationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('fdt_migration');

        $rootNode
            ->children()
            ->scalarNode('source_file_path')
            ->defaultValue('/home/ad/Vidéos/migrator.sql')
            ->end()
            ->scalarNode('log_file_path')
            ->defaultValue('/home/ad/Vidéos/log.txt')
            ->end()
            ->scalarNode('target_file_path')
            ->defaultValue('/home/ad/Vidéos/target.sql')
            ->end()
            ->end();


        return $treeBuilder;
    }
}