<?php


namespace App\Fdt\MigrationBundle;
use App\Fdt\MigrationBundle\DependencyInjection\MigrationExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MigrationBundle extends Bundle
{


    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new MigrationExtension();
        }
        return $this->extension;
    }




}
