<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 17/04/18
 * Time: 09:10
 */

namespace App\Fdt\MigrationBundle\Controller;


use App\Fdt\MigrationBundle\Migration\MigrateCreateQuery;
use App\Fdt\MigrationBundle\Migration\MigrationByFile;
use FOS\RestBundle\Controller\Annotations as Rest;


class MigrationController
{
    /**
     * @param  $sqlQuery
     * @Rest\Get("/migrateCreateQuery/{sqlQuery}")
     */
    public function getAction($sqlQuery)
    {
        $migrateCreateQuery=new MigrateCreateQuery();
        dump($migrateCreateQuery->migrateCreateTable($sqlQuery));
        die;
    }
    /**
     *
     * @Rest\Get("/migrateFile")
     */
    public function getAction1()
    {
        $MigrateFile=new MigrationByFile();
        dump($MigrateFile->migrateFile());
        die;
    }



}