<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 13/04/18
 * Time: 16:30
 */

namespace App\Fdt\MigrationBundle\Migration;


class MigrateColumn extends MigrateCreateQuery
{

    /**
     * @param void
     */
    public function __construct()
    {
    }

    /**
     * @param $column
     * @return string
     */

    public  function migrateColumn($column)
    {
        $column =$this->cleanColumn($column);

        $nameColumn = strstr($column, ' ', true);

        $typeAndConstraint = trim(strstr($column, ' '));

        $Constraint = $this->separateColunmElement($typeAndConstraint)[0];

        $types = $this->separateColunmElement($typeAndConstraint)[1];

        return $nameColumn . $types . $Constraint;
    }

    /**
     * @param $column
     * @return mixed
     */
    public  function cleanColumn($column)
    {

        $column = str_ireplace(['UNSIGNED ZEROFILL', 'UNSIGNED'], '', $column);
        $posCollate = stripos($column, 'collate');
        if ($posCollate !== false) {
            $encodingColumn = substr($column, $posCollate);
            $encodingColumn = preg_replace("# +#", " ", trim($encodingColumn));

            $encodeArray = explode(' ', $encodingColumn);
            $encoding = $encodeArray[1];


            $column = str_ireplace(['collate', $encoding], '', $column);
            $column = preg_replace("# +#", " ", trim($column));


        }
        return $column;
    }


    /**
     * @param $typeAndConstraint
     * @return array
     */
    public  function separateColunmElement($typeAndConstraint)
    {
        $migrateTypes=new MapType();


        if (strpos($typeAndConstraint, '(') !== false) {

            $type = strstr($typeAndConstraint, '(', true);

            $sizeType = $this->separateConstraintAndSize($typeAndConstraint)[1];

            $Constraint = $this->separateConstraintAndSize($typeAndConstraint)[0];

            $type = $type . $sizeType;

            $types = $migrateTypes->migrateTypes($type);


        } else {


            $type = strstr($typeAndConstraint, ' ', true);

            $Constraint = trim($typeAndConstraint, $type);

            $types = $migrateTypes->migrateTypes($type);


        }

        return array($Constraint, $types);
    }

    /*identifie the type size and constraint
     * @param $typeAndConstraint
     * @return array
     */
    public  function separateConstraintAndSize($typeAndConstraint)

    {
        $sizeAndConstraint = strstr($typeAndConstraint, '(');

        if (stripos($sizeAndConstraint, ' ') !== false) {
            $sizeType = strstr($sizeAndConstraint, ' ', true);
            $Constraint = trim(trim($sizeAndConstraint, $sizeType));
        } else {

            $sizeType = $sizeAndConstraint;

            $Constraint = '';
        }

        return array($Constraint, $sizeType);
    }

}
