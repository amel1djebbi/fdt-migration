<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 13/04/18
 * Time: 15:50
 */

namespace App\Fdt\MigrationBundle\Migration;


class MigrateStructure
{


    /**
     *
     * @param void
     */
    public function __construct()
    {
    }

    /**separate create index query from create table query
     * @param $reqSql
     * @return mixed|string
     */

    public function separateIndexes($reqSql)
    {
        if (stripos($reqSql, ', create') !== false) {

            $reqSqlTemp = str_ireplace(', create', ' ); create', $reqSql);

            $reqSql = $reqSqlTemp;
            $arraySql = explode(';', $reqSql);
            $reqSql = implode(';' . PHP_EOL, $arraySql);

            return $reqSql;
        } else

            return $reqSql;
    }

    /**
     * @param $sqlQuery
     * @return string
     */
    public function migrateDropQuery($sqlQuery)
    {
        $sqlQuery = Util::CleanQuery($sqlQuery);
        return $sqlQuery . ';';
    }

}

