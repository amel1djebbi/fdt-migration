<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 17/04/18
 * Time: 10:12
 */

namespace App\Fdt\MigrationBundle\Migration;


class Util
{
    /**
     * @param $sqlQuery
     * @return mixed|string
     */
    public static function CleanQuery($sqlQuery)
    {
        $sqlQuery = str_replace('`', '', $sqlQuery);
//replace successif space with one space
        $sqlQuery = trim(preg_replace("# +#", " ", trim($sqlQuery)));


        return $sqlQuery;
    }

}