<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 13/04/18
 * Time: 14:18
 */

namespace App\Fdt\MigrationBundle\Migration;


/**
 * Class MigrateData
 * @package App\metier
 */
class MigrateData
{


    /**
     *
     * @param void
     */
    public function __construct()
    {
        // No code should be put here.
    }

    /**
     * @param $sqlQuery
     * @return string
     */

    public  function migrateReplaceQuery($sqlQuery)
    {
        $sqlQuery=Util::CleanQuery($sqlQuery);

        $arraySql = explode(' ', $sqlQuery);

        $tableName = $arraySql[2];
        $columnName = $this->getcolumnName($sqlQuery);
        $columnValue = $this->getcolunmValue($sqlQuery);
        $pattern = '/^[(][a-z]|[A-Z]/';
        if (preg_match($pattern, $columnName)) {

            return 'INSERT INTO ' . $tableName . ' ' . $columnName . ')' . ' VALUES ' . $columnValue . ';';
        } else
            return 'INSERT INTO ' . $tableName . ' ' . ' VALUES ' . $columnValue . ' ;';


    }



    /**
     * @param $sqlQuery
     * @return string
     */
    public  function getcolumnName($sqlQuery)
    {
        $columnName = trim(strstr(trim(substr($sqlQuery, strpos($sqlQuery, '('))), ')', true));

        return $columnName;
    }

    /**
     * @param $sqlQuery
     * @return string
     */
    public  function getcolunmValue($sqlQuery)
    {
        $columnValue = strstr(strstr($sqlQuery, 'VALUES'), '(');
        return $columnValue;
    }
    /**
     * @param $sqlQuery
     * @return string
     */
    public  function migrateInsertQuery($sqlQuery)
    {
        $sqlQuery=Util::CleanQuery($sqlQuery);
        return $sqlQuery . ';';
    }

    /**
     * @param $sqlQuery
     * @return string
     */
    public  function migrateUpdateQuery($sqlQuery)
    {
        $sqlQuery=Util::CleanQuery($sqlQuery);

        return $sqlQuery . ';';
    }

}