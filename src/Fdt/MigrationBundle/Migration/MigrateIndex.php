<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 13/04/18
 * Time: 17:13
 */

namespace App\Fdt\MigrationBundle\Migration;


class MigrateIndex
{

    /**
     *
     * @param void
     */
    public function __construct()
    {
        // No code should be put here.
    }
    /**
     * @param $column
     * @param $tableName
     * @return string
     */
    public  function migrateIndex($column, $tableName)
    {
        $column = $this->findIndex($column);


        $createIndexStr=$this->findCreateIndexStr($column);


        $index = 'CREATE ' . $createIndexStr . ' on ' . $tableName . $this->findFieldName($column);

        return $index;
    }

    /**
     * @param $column
     * @return string
     */
    public static function findIndex($column)
    {

        $column = trim($column, 'index');
        $column = trim(preg_replace("# +#", " ", trim($column)));
        return $column;
    }

    /**
     * @param $column
     * @return string
     */
    public static function findCreateIndexStr($column)
    {
        $createFieldPos = strpos($column, "(");

        $createIndexStr = trim(substr($column, 0, $createFieldPos));

        return $createIndexStr;
    }

    /**
     * @param $column
     * @return string
     */
    public static function findFieldName($column){
        $fieldName = trim(substr($column, strrpos($column, ' ')));

        return $fieldName;

    }
}




