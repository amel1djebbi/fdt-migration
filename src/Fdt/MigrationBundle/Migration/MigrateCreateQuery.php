<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 13/04/18
 * Time: 14:58
 */

namespace App\Fdt\MigrationBundle\Migration;


use Exception;

/**
 * Class MigrateCreateQuery
 * @package App\metier
 */
class MigrateCreateQuery extends  MigrateStructure
{
    /**
     *
     * @param void
     */
    public  function __construct()
    {
    }
    /**
     * @param $sqlQuery
     * @return mixed|string
     * @throws Exception
     */
    public  function migrateCreateTable($sqlQuery)
    {

        $sqlQuery =Util::CleanQuery($sqlQuery);

        $this->verifQuery($sqlQuery);

        $createTableStr = $this->findcreateTableStr($sqlQuery);

        $tableName = $this->verifAndFindTableName($createTableStr);

        $fields = $this->findFields($sqlQuery);

        $fields = $this->migrateFields($fields, $tableName);

//delete tablName from createTableStr
        $createTableStr = rtrim($createTableStr, $tableName);


        $reqSql = sprintf($createTableStr . ' %s (%s;', $tableName, implode(', ', $fields));


        $reqSql =  $this->separateIndexes($reqSql);

        return $reqSql;
    }

    /**verif query syntax
     * @param $sqlQuery
     * @throws Exception
     */
    public  function verifQuery($sqlQuery)
    {
        if (strpos($sqlQuery, '(') === false || strpos($sqlQuery, ')') === false) {
            throw new Exception(' syntax error');

        }

    }


    /** find create create table syntax
     * @param $sqlQuery
     * @return string
     */
    public  function findcreateTableStr($sqlQuery)
    {
        $createFieldPos = strpos($sqlQuery, "(");

        $createTableStr = trim(substr($sqlQuery, 0, $createFieldPos));

        return $createTableStr;
    }

    /**find and verif table name
     * @param $createTableStr
     * @return string
     * @throws Exception
     */
    public  function verifAndFindTableName($createTableStr)
    {
        $tableName = trim(substr($createTableStr, strrpos($createTableStr, ' ')));

      /*  if ($tableName == 'EXISTS' || $tableName == '' || $tableName == 'table' || $tableName == 'if' || $tableName = 'not') {
            throw new Exception(' syntax error :you shouls add table name');

        }*/
     return $tableName;

    }

    /**find all fields
     * @param $sqlQuery
     * @return array
     */
    public  function findFields($sqlQuery)
    {
        $createFieldPos = strpos($sqlQuery, "(");

        $createFieldsStr = trim(substr($sqlQuery, $createFieldPos + 1));


        $createFieldsStr = rtrim($createFieldsStr, ';');

        $createFieldsStr = substr($createFieldsStr, 0, -1);

        $fields = explode(',', $createFieldsStr);

        return $fields;
    }


    /**migrate all fields with indexes
     * @param $fields
     * @param $tableName
     * @return mixed
     */
    public  function migrateFields($fields, $tableName)
    {
        $migrateIndex=new MigrateIndex();
        $migrateColumn=new MigrateColumn();

        foreach ($fields as $key => $field) {


            if ((strpos($field, 'index') === false) &&
                (strpos($field, 'primary key') === false) &&
                (strpos($field, 'foreign key') === false)) {
                $fields[$key] = $field = $migrateColumn->migrateColumn($field);


            } elseif (strpos($field, 'index') !== false) {

                $fields[$key] = $migrateIndex->migrateIndex($field, $tableName);

            } elseif (strpos($field, 'primary key') === true || strpos($field, 'foreign key ') === true) {

                $fields[$key] = $field;
            }


        }
        return $fields;
    }


}