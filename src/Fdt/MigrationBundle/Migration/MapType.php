<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 29/03/18
 * Time: 08:45
 */

namespace App\Fdt\MigrationBundle\Migration;


class MapType
{

    /**
     * The purpose of explicit private constructor is
     * to prevent an instance initialization.
     *
     * @param void
     */
    public function __construct()
    {
        // No code should be put here.
    }

    /**
     * Dictionary of MySql data types with corresponding PostgreSql data types.
     *
     * @var array
     */
    public $arrTypesMap = [
        'json' => [
            'increased_size' => '',
            'type' => 'json',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'bit' => [
            'increased_size' => 'bit varying',
            'type' => 'bit varying',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'year' => [
            'increased_size' => 'int',
            'type' => 'smallint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'tinyint' => [
            'increased_size' => 'int',
            'type' => 'smallint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'smallint' => [
            'increased_size' => 'int',
            'type' => 'smallint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'mediumint' => [
            'increased_size' => 'bigint',
            'type' => 'int',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'int' => [
            'increased_size' => 'bigint',
            'type' => 'int',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'bigint' => [
            'increased_size' => 'bigint',
            'type' => 'bigint',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'float' => [
            'increased_size' => 'double precision',
            'type' => 'real',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'double' => [
            'increased_size' => 'double precision',
            'type' => 'double precision',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'double precision' => [
            'increased_size' => 'double precision',
            'type' => 'double precision',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'numeric' => [
            'increased_size' => '',
            'type' => 'numeric',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'decimal' => [
            'increased_size' => '',
            'type' => 'decimal',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'decimal(19,2)' => [
            'increased_size' => 'numeric',
            'type' => 'money',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'char' => [
            'increased_size' => '',
            'type' => 'character',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'varchar' => [
            'increased_size' => '',
            'type' => 'character varying',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'date' => [
            'increased_size' => '',
            'type' => 'date',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'time' => [
            'increased_size' => '',
            'type' => 'time',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'datetime' => [
            'increased_size' => '',
            'type' => 'timestamp',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'timestamp' => [
            'increased_size' => '',
            'type' => 'timestamp',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'point' => [
            'increased_size' => '',
            'type' => 'point',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'multipoint' => [
            'increased_size' => '',
            'type' => 'point[]',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'linestring' => [
            'increased_size' => '',
            'type' => 'line',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'multilinestring' => [
            'increased_size' => '',
            'type' => 'line[]',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'polygon' => [
            'increased_size' => '',
            'type' => 'polygon',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'multipolygon' => [
            'increased_size' => '',
            'type' => 'polygon[]',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'enum' => [
            'increased_size' => '',
            'type' => 'character varying(255)',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'set' => [
            'increased_size' => '',
            'type' => 'character varying(255)',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'tinytext' => [
            'increased_size' => '',
            'type' => 'text',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'mediumtext' => [
            'increased_size' => '',
            'type' => 'text',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'longtext' => [
            'increased_size' => '',
            'type' => 'text',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'text' => [
            'increased_size' => '',
            'type' => 'text',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'varbinary' => [
            'increased_size' => '',
            'type' => 'bytea',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'binary' => [
            'increased_size' => '',
            'type' => 'bytea',
            'mySqlVarLenPgSqlFixedLen' => true,
        ],
        'tinyblob' => [
            'increased_size' => '',
            'type' => 'bytea',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'mediumblob' => [
            'increased_size' => '',
            'type' => 'bytea',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'longblob' => [
            'increased_size' => '',
            'type' => 'bytea',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'blob' => [
            'increased_size' => '',
            'type' => 'bytea',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],
        'boolean' => [
            'increased_size' => '',
            'type' => 'boolean',
            'mySqlVarLenPgSqlFixedLen' => false,
        ],

        'serial' => [
            'increased_size' => '',
            'type' => 'serial',
            'mySqlVarLenPgSqlFixedLen' => false,]

    ];

    /**
     *
     * @param  string $strMySqlDataType
     * @return string
     */
    public function migrateTypes($strMySqlDataType)
    {
        $strRetVal = '';
        $arrDataTypeDetails = explode(' ', $strMySqlDataType);
        $boolIncreaseOriginalSize = in_array('UNSIGNED', $arrDataTypeDetails) || in_array('zerofill', $arrDataTypeDetails);
        $strMySqlDataType = $arrDataTypeDetails[0];
        $strMySqlDataType = strtolower($strMySqlDataType);
        $parenthesesFirstOccurrence = strpos($strMySqlDataType, '(');
        if (false === $parenthesesFirstOccurrence) {
            // No parentheses detected.
            $strRetVal = $boolIncreaseOriginalSize
                ? $this->arrTypesMap[$strMySqlDataType]['increased_size']
                : $this->arrTypesMap[$strMySqlDataType]['type'];
        } else {
            // Parentheses detected.
            $arrDataType = explode('(', $strMySqlDataType);
            $strDataType = strtolower($arrDataType[0]);
            if ('enum' == $strDataType || 'set' == $strDataType) {
                $strRetVal = 'varchar(255)';
            } elseif ('decimal' == $strDataType || 'numeric' == $strDataType) {
                $strRetVal = $this->arrTypesMap[$strDataType]['type'] . '(' . $arrDataType[1];
            } elseif ('decimal(19,2)' == $strMySqlDataType) {
                $strRetVal = $boolIncreaseOriginalSize
                    ? $this->arrTypesMap[$strDataType]['increased_size']
                    : $this->arrTypesMap[$strDataType]['type'];
            } elseif ($this->arrTypesMap[$strDataType]['mySqlVarLenPgSqlFixedLen']) {
                // Should be converted without a length definition.
                $strRetVal = $boolIncreaseOriginalSize
                    ? $this->arrTypesMap[$strDataType]['increased_size']
                    : $this->arrTypesMap[$strDataType]['type'];
            } else {
                // Should be converted with a length definition.
                $strRetVal = $boolIncreaseOriginalSize
                    ? $this->arrTypesMap[$strDataType]['increased_size'] . '(' . $arrDataType[1]
                    : $this->arrTypesMap[$strDataType]['type'] . '(' . $arrDataType[1];
            }
            // Prevent incompatible length (CHARACTER(0) or CHARACTER VARYING(0)).
            switch ($strRetVal) {
                case 'character(0)':
                    $strRetVal = 'character(1)';
                    break;
                case 'character varying(0)':
                    $strRetVal = 'character varying(1)';
                    break;
            }
        }

        return ' ' . strtoupper($strRetVal) . ' ';
    }


}