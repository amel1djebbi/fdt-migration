<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 13/04/18
 * Time: 14:37
 */

namespace App\Fdt\MigrationBundle\Migration;


/**
 * Class MigrateStructure
 * @package App\metier
 */
class MigrateAlterQuery extends MigrateStructure
{
    /**
     *
     * @param void
     */
    public function __construct()
    {
    }

    /**
     * @param $sqlQuery
     * @return mixed|string
     */
    public function migrateAlter($sqlQuery)
    {

        $sqlQuery = Util::CleanQuery($sqlQuery);

        $tableName = $this->findTableName($sqlQuery);

        $arrayConstraint = $this->findConstraints($sqlQuery, $tableName);


        $reqSql = 'alter table ' . $tableName . ' ' . implode(', ', $arrayConstraint);
        $reqSql = $this->separateIndexes($reqSql);
        return $reqSql;
    }

    /**
     * @param $sqlQuery
     * @return string
     */
    public function findTableName($sqlQuery)
    {
        $alterTableStr = trim(strstr($sqlQuery, ['ADD', 'MODIFY'], true));
        $tableName = trim(substr($alterTableStr, strrpos($alterTableStr, ' ')));
        return $tableName;
    }

    /** return arrayConstraints
     * @param $sqlQuery
     * @return array
     */
    public function findConstraints($sqlQuery, $tableName)
    {
        $migrateIndex = new MigrateIndex();

        $constraints = strstr($sqlQuery, 'ADD');

        $arrayConstraint = explode(',', $constraints);

        foreach ($arrayConstraint as $key => $constraint) {

            $constraint = trim(trim($constraint), 'ADD');

            if (stripos($constraint, 'unique key') !== false || stripos($constraint, 'index') !== false || stripos($constraint, 'unique index') !== false) {

                $constraint = $migrateIndex->migrateIndex($constraint, $tableName);

                $arrayConstraint[$key] = $constraint;


            }
        }

        return $arrayConstraint;
    }

}