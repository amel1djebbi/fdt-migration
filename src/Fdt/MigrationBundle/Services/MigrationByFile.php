<?php
/**
 * Created by PhpStorm.
 * User: ad
 * Date: 17/04/18
 * Time: 08:23
 */

namespace App\Fdt\MigrationBundle\Services;

use App\Migration\MigrateAlterQuery;
use App\Migration\MigrateCreateQuery;
use App\Migration\MigrateData;
use App\Migration\MigrateStructure;
use App\Migration\Util;
use Exception;

class MigrationByFile
{
    /**
     * @throws Exception
     */
    public function migrateFile($source,$target,$strategy)
    {
        $queries = array();
     //   $strategy = 'data';
        $query = '';
       // $source = '/home/ad/Vidéos/migrator.sql';
        //$target = '/home/ad/Vidéos/target1.sql';
        $subStrategy = 'do not update data';
        @unlink($target);

        if ($fichier = @fopen($source, "r")) {
            $lines = file($source);
            foreach ($lines as $lineNumber => $lineContent) {

                $lineContent = self::cleanLineFile($lineContent);

                $lineLength = strlen($lineContent);
                $endQueryPosition = strpos($lineContent, ';');

                $query .=
                    substr($lineContent, 0, ((!$endQueryPosition) ? $lineLength : $endQueryPosition));

                if ($endQueryPosition) {
                    $queries[] = str_replace("\n", ' ', $query);
                    $query = '';
                }
            }

            $this->findMigrateQuery($queries, $strategy, $target, $subStrategy);

        }
    }


    /**
     * @param $filename
     * @param array $queries
     */
    public function writeQueries($filename, array $queries)
    {
        $targetFile = fopen($filename, "w+");
        foreach ($queries as $query) {
            $query = $query . PHP_EOL;

            fwrite($targetFile, $query);

        }
        fclose($targetFile);
    }


    /**
     * @param $line
     * @return bool
     */
    public function isValidLineFile($line)
    {
        $pattern = '/^SET/';

        if ((stripos($line, '/*') !== false) ||
            (stripos($line, '--') !== false) ||
            (preg_match($pattern, $line)) ||
            (preg_match('/^UNLOCK/', $line)) ||
            (preg_match('/^LOCK/', $line)) ||
            (!strlen($line))) {

            RETURN false;

        } else
            RETURN true;
    }


    /**
     * @param $line
     * @return string
     */
    public function cleanLineFile($line)
    {
        if (stripos($line, ') ENGINE') !== false) {
            $line = trim(str_ireplace($line, ');', $line));
            return $line;
        } elseif ($this->isValidLineFile($line) == true) {
            return $line;
        } else {
            $line = trim(str_ireplace($line, '', $line));
            return $line;
        }


    }

    /**
     * @param $query
     * @return string
     */
    public function migrateStructure($query)
    {
        $migrateCreateQuery = new MigrateCreateQuery();
        $migrateAlterQuery = new MigrateAlterQuery();
        $migrateDrop = new MigrateStructure();
        if (stripos($query, 'create table') !== false) {
            $query = $migrateCreateQuery->migrateCreateTable($query);

        } elseif (stripos($query, 'alter table') !== false) {
            $query = $migrateAlterQuery->migrateAlter($query);

        } elseif (stripos($query, 'drop table') !== false) {
            $query = $migrateDrop->migrateDropQuery($query);

        } else

            return '';

        return $query;
    }

    /**
     * @param $query
     * @param $subStrategy
     * @return string
     */
    public function migrateData($query, $subStrategy)
    {
        $migrateUpdate = new MigrateData();
        $migrateUpdate = new MigrateData();
        $query = str_replace('`', '', $query);
        if (stripos($query, 'insert into ') !== false) {
            $tableName = $this->extractInsertTableName($query);

            $query1 = $this->truncateTable($tableName);
            $query = $this->ChoiceSubStrategy($query, $subStrategy);
        } elseif (stripos($query, 'update') !== false) {//to do
            $tableName = $this->getUpdateTableName($query);
            $query1 = $this->truncateTable($tableName);
            $query = $migrateUpdate->migrateUpdateQuery($query);

        } elseif (stripos($query, 'replace into ') !== false) {
            $tableName = $this->extractInsertTableName($query);
            $query1 = $this->truncateTable($tableName);
            $query = $this->ChoiceSubStrategy($query, $subStrategy);

        } else

            return '';
        return $query1 . PHP_EOL . $query;
    }

    public function migrateStructureAndData($query)
    {
        $migrateCreateQuery = new MigrateCreateQuery();
        $migrateAlterQuery = new MigrateAlterQuery();
        $migrateDrop = new MigrateStructure();
        $migrateData = new MigrateData();
        if (stripos($query, 'insert into') !== false) {
            $query = $migrateData->migrateInsertQuery($query);

        } elseif (stripos($query, 'update') !== false) {
            $query = $migrateData->migrateUpdateQuery($query);


        } elseif (stripos($query, 'replace into ') !== false) {
            $query = $migrateData->migrateReplaceQuery($query);

        }
        if (stripos($query, 'create table') !== false) {
            $query = $migrateCreateQuery->migrateCreateTable($query);

        } elseif (stripos($query, 'alter table') !== false) {
            $query = $migrateAlterQuery->migrateAlter($query);

        } elseif (stripos($query, 'drop table') !== false) {
            $query = $migrateDrop->migrateDropQuery($query);

        }


        return $query;
    }

    /**
     * @param $tableName
     * @return string
     */
    public static function truncateTable($tableName)
    {
        return 'TRUNCATE TABLE ' . $tableName . ' ;';
    }

    public function extractInsertTableName($query)
    {
        $createFieldPos = strpos($query, "(");

        $createTableStr = trim(substr($query, 0, $createFieldPos));

        $tableName = trim(substr($createTableStr, strrpos($createTableStr, ' ')));
        return $tableName;
    }

    public function getUpdateTableName($query)
    {
        $createFieldPos = stripos($query, "set");

        $createTableStr = trim(substr($query, 0, $createFieldPos));

        $tableName = trim(substr($createTableStr, strrpos($createTableStr, ' ')));


        return $tableName;
    }


    public function ChoiceSubStrategy($query, $subStrategy)
    {
        $migrateData = new MigrateData();
        if ($subStrategy == 'do not update data') {
            $query = rtrim($migrateData->migrateInsertQuery($query), ';');
            $query = $query . ' ON CONFLICT DO nothing ; ';
        } else {
            $query = $migrateData->migrateInsertQuery($query);
        }

        if ($subStrategy == 'update data') {

            $query = $migrateData->migrateInsertQuery($query);


        }
        return $query;

    }

    public static function findIndexUnique($query, $tableName)/// to do
    {
        if (stripos($query, $tableName))
            return $query;
    }


    public function findMigrateQuery($queries, $strategy, $target, $subStrategy)
    {
        $migratedQueries = [];

        foreach ($queries as $key => $query) {
            $query = Util::CleanQuery($query);
            if (count($queries) < 50) {
                if ($strategy == 'structure') {
                    $migratedQueries[$key] = $this->migrateStructure($query);
                    $this->writeQueries($target, $migratedQueries);

                } elseif ($strategy == 'data') {
                    $migratedQueries[$key] = $this->migrateData($query, $subStrategy);
                    $this->writeQueries($target, $migratedQueries);
                } elseif ($strategy == 'structure and data') {
                    $migratedQueries[$key] = $this->migrateStructureAndData($query);
                    $this->writeQueries($target, $migratedQueries);
                }
                $this->writeQueries($target, $migratedQueries);
            } elseif (count($queries) == 50) {
                $queries = array();


            }
        }
    }


}









