<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServerAccountRepository")
 * @UniqueEntity("code")
 */
class ServerAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
* @ORM\Column(type="string")
     */
    private $host;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $code;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $username;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $password;
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $SGBDType;

    /**
* @ORM\OneToMany(targetEntity="App\Entity\DataBase", mappedBy="serverAccount")
*/
    private $dataBase;

    /**
     *
     */
    public function __construct1()
    {
        $this->dataBase = new ArrayCollection();
    }

    /**
     * @return Collection|dataBase]
     */
    public function getDataBase()
    {
        return $this->dataBase;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $hote
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**MySQL
     * @return mixed
     */
    public function getSGBDType()
    {
        return $this->SGBDType;
    }

    /**
     * @param mixed $SGBDType
     */
    public function setSGBDType($SGBDType)
    {
        $this->SGBDType = $SGBDType;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * ServerAccount constructor.
     */
    public function __construct()
    {
        $this->serverAccounts = new ArrayCollection();
    }

    /**
     * @param ServerAccount $serverAccount
     * @return $this
     */
    public function addServerAccount(ServerAccount $serverAccount)
    {
        $this->serverAccounts[] = $serverAccount;

        return $this;
    }

    /**
     * @param ServerAccount $serverAccount
     */
    public function removeServerAccount(ServerAccount $serverAccount)
    {
        $this->serverAccounts->removeElement($serverAccount);
    }
    /**
     * @return ArrayCollection
     */
    public function getServerAccount()
    {
        return $this->serverAccounts;
    }
}
