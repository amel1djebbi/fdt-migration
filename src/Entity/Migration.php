<?php

namespace App\Entity;
use App\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MigrationRepository")
 */
class Migration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $label;
    /**
     * @ORM\Column(type="string")
     */
    private $sourceType;
    /**
     * @ORM\Column(type="string")
     */
    private $encodingType;
    /**
     * @ORM\Column(type="string")
     */
    private $mysqlVersion;
    /**
     *
     * @ORM\Column(type="string")
     */
    private $postgresqlVersion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LogMigration", mappedBy="migration")
     */
    private $logMigration;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Strategy", inversedBy="migration")
     * @ORM\JoinColumn(nullable=true)
     */
    private $strategy;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File", cascade={"persist", "merge", "remove"})
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private  $file ;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DataBase", inversedBy="migration")
     * @ORM\JoinColumn(nullable=true)
     */
    private $dataBase;

    /**
     * Migration constructor.
     */
    public function __construct1()
    {
        $this->logMigration = new ArrayCollection();
    }

    /**
     * @return Collection|logMigration]
     */
    public function getLogMigration()
    {
        return $this->logMigration;
    }

    /**
     * @return strategy
     */
    public function getStrategy(): strategy
    {
        return $this->strategy;
    }

    /**
     *
     *
     * @param Strategy $strategy
     *
     */
    public function setStrategy(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param File $file
     */
    public function setFile(File $file)
    {
        $this->file = $file;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->File;
    }


    /**
     * @return dataBase
     */
    public function getDataBase(): dataBase
    {
        return $this->dataBase;
    }

    /**
     * @param DataBase $dataBase
     */
    public function setDataBase(DataBase $dataBase)
    {
        $this->dataBase = $dataBase;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getSourceType()
    {
        return $this->sourceType;
    }

    /**
     * @param mixed $TypeSource
     */
    public function setSourceType($sourceType)
    {
        $this->sourceType = $sourceType;
    }

    /**
     * @return mixed
     */
    public function getEncodingType()
    {
        return $this->encodingType;
    }

    /**
     * @param mixed $encodingType
     */
    public function setEncodingType($encodingType)
    {
        $this->encodingType = $encodingType;
    }

    /**
     * @return mixed
     */
    public function getMysqlVersion()
    {
        return $this->mysqlVersion;
    }

    /**
     * @param mixed $mysqlVersion
     */
    public function setMysqlVersion($mysqlVersion)
    {
        $this->mysqlVersion = $mysqlVersion;
    }

    /**
     * @return mixed
     */
    public function getPostgresqlVersion()
    {
        return $this->postgresqlVersion;
    }

    /**
     * @param mixed $postgresqlVersion
     */
    public function setPostgresqlVersion($postgresqlVersion)
    {
        $this->postgresqlVersion = $postgresqlVersion;
    }

    /**
     * Migration constructor.
     */
    public function __construct()
    {
        $this->migrations = new ArrayCollection();
    }


    /**
     * @param Migration $migration
     * @return $this
     */
    public function addMigration(Migration $migration)
    {
        $this->migrations[] = $migration;

        return $this;
    }

    /**
     * @param Migration $migration
     */
    public function removeMigration(Migration $migration)
    {
        $this->migrations->removeElement($migration);
    }

    /**
     * @return ArrayCollection
     */
    public function getMigration()
    {
        return $this->migrations;
    }
    public function createMigration()
    {
return 'migration create ';
    }
}








