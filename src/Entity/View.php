<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ViewRepository")
 */
class View
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
* @ORM\Column(type="string")
*/
    private $viewName ;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DataBaseTable", inversedBy="view")
     * @ORM\JoinColumn(nullable=true)
     */
    private $dataBaseTable;

    /**
     * @return dataBaseTable
     */
    public function getDataBaseTable(): dataBaseTable
    {
        return $this->dataBaseTable;
    }

    /**
     * @return mixed
     */
    public function getViewName()
    {
        return $this->viewName;
    }

    /**
     * @param mixed $viewName
     */
    public function setViewName($viewName)
    {
        $this->viewName = $viewName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param DataBaseTable $DataBaseTable
     */
    public function setDataBaseTable(DataBaseTable $dataBaseTable)
    {
        $this->dataBaseTable = $dataBaseTable;
    }

}
